This repository contains the needed code and metadata files to repeat the analysis shown in Ledergor et al. "Single cell dissection of plasma cell heterogeneity in symptomatic and asymptomatic myeloma", Nature Medicine 2018

In order to run the scripts, downloaded processed data from the GSEXXXX needs to be added in specific folders: Processed UMI-tab files (ABXXX.txt) from GSE92575 should be copied to the folder output/umi.tab

The matlab part requires PhenoGraph matlab wrapper from Dana Peer's lab (https://github.com/dpeerlab/cyt3)

R scripts uses MetaCell package from Amos Tanay's lab (https://bitbucket.org/tanaylab/metacell/)

Please send questions to Assaf Weiner: assaf.weiner@weizmann.ac.il