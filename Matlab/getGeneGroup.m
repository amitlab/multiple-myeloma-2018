function [final_genes_group] = getGeneGroup(n_genes,MAP,SCData,black_list)
	% Find genes which separate the groups
	min_umi=2;
	inds = find(~isnan(MAP)&MAP>0);
	exp_counts = sum(SCData.DS(:,inds),2);
	exp_counts = repmat(exp_counts / sum(exp_counts),1,size(SCData.DS(:,inds),2));
	exp_counts = exp_counts .* repmat(sum(SCData.DS(:,inds),1),size(SCData.DS(:,inds),1),1);

	ex = zeros(max(MAP),size(exp_counts,1));
	for i=1:size(exp_counts,1),
	    ex(:,i)=accumarray(MAP(inds)',exp_counts(i,:)');
	end

	obs= zeros(max(MAP),size(SCData.DS(:,inds),1));
	for i=1:size(SCData.DS(:,inds),1),
	    obs(:,i)=accumarray(MAP(inds)',SCData.DS(i,inds)');
	end
	ex = ex(2:end,:)'; obs = obs(2:end,:)';

	x2 = sum(((obs-ex).^2 )./ex,2); % chi^2 with df = ncluster-1

	x2pdf = chi2pdf(x2,max(MAP)-1);

	var_mean=var(SCData.DS(:,inds),0,2) ./ mean(SCData.DS(:,inds),2);
	var_mean((sum(SCData.DS(:,inds),2)/length(inds))<0.01) = 0;

	x2(isnan(x2)) = 0;
	n_cells_exp = sum(SCData.DS(:,inds)>min_umi,2);
	x2(n_cells_exp<10) = 0;
	[tmp order] = sort(x2,'descend');
	x2(var_mean<1) = 0;

	% Filter genes
	to_filter_inds = [];
	for i=1:length(black_list)
	    to_filter_inds = union(to_filter_inds,...
	        find(~cellfun('isempty',regexp(SCData.genes,black_list{i},'match'))));
	end

	x2(to_filter_inds)=0;
	[tmp order] = sort(x2,'descend');
	final_genes_group = order(1:n_genes);
