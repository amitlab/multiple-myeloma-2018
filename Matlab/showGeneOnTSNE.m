function [] = showGeneOnTSNE(mapped,SCData,gname,msize)
    figure;
    g = strmatch(gname,SCData.genes,'exact');
    if (isempty(g))
        error('Wrong gene name');
        return
    end
    scatter(mapped(:,1),mapped(:,2),msize,log2(1+SCData.DS(g,:)),'filled');
    load CoDM1;
    colormap(CoDM1);
    alpha 0.7;
end