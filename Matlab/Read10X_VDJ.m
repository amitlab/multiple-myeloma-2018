function [SCData] = Read10X_VDJ(SCData,dir_name)

    % Read csv
    bcr_table = readtable([dir_name '/filtered_contig_annotations.csv']);
    
    % barcode to cell
    barcodeMap = containers.Map();
    for i=1:length(SCData.barcodes)
        barcodeMap(SCData.barcodes{i}) = i;
    end
    
    % Assign clonetype per cell
    SCData.clonetype = {};
    SCData.lightChain = {};
    SCData.lightReads = [];
    SCData.lightUMIs = [];
    SCData.heavyChain = {};
    SCData.heavyReads = [];
    SCData.heavyUMIs = [];
    for i=1:length(SCData.barcodes)
        SCData.clonetype{i} = 'none';
        SCData.lightChain{i} = 'none';
        SCData.lightReads(i) = 0;
        SCData.lightUMIs(i) = 0;
        SCData.heavyChain{i} = 'none';
        SCData.heavyReads(i) = 0;
        SCData.heavyUMIs(i) = 0;
    end
    
    for i=1:length(bcr_table.barcode)
        if (strcmp(bcr_table.raw_consensus_id{i},'None')), continue; end
        if (~isKey(barcodeMap,bcr_table.barcode{i})), continue; end
        cur_cell = barcodeMap(bcr_table.barcode{i});
        SCData.clonetype{cur_cell} = bcr_table.raw_clonotype_id{i};
        if (strcmp(bcr_table.chain{i},'IGK') || strcmp(bcr_table.chain{i},'IGL'))
            SCData.lightChain{cur_cell} = [bcr_table.v_gene{i},':',bcr_table.j_gene{i},':',bcr_table.c_gene{i},':',bcr_table.cdr3{i}];
            SCData.lightReads(cur_cell) = bcr_table.reads(i);
            SCData.lightUMIs(cur_cell) = bcr_table.umis(i);
        else
            SCData.heavyChain{cur_cell} = [bcr_table.v_gene{i},':',bcr_table.d_gene{i},':',bcr_table.j_gene{i},':',bcr_table.c_gene{i},':',bcr_table.cdr3{i}];
            SCData.heavyReads(cur_cell) = bcr_table.reads(i);
            SCData.heavyUMIs(cur_cell) = bcr_table.umis(i);
        end
    end
    SCData.clone_id = zeros(size(SCData.barcodes));
    for i=1:length(SCData.clonetype)
        if (strcmp(SCData.clonetype{i},'none')), continue; end
        SCData.clone_id(i) = str2num(SCData.clonetype{i}(10:end));
    end
end
