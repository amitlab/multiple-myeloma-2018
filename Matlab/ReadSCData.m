% READ Mars-seq umi tab
% batches - batches numbers to read from umi directory
function [SCData] = ReadSCData(batches_numbers,varargin)


    args = parse_namevalue_pairs(struct('umi_dir','/Volumes/eyald/sc_pipeline/scdb_hisat_mouse/output/umi.tab/'),varargin);

    umi_dir = args.umi_dir;

    batches = arrayfun(@strcat,repmat({'AB'},length(batches_numbers),1),strread(num2str(batches_numbers),'%s'));

    tmp = importdata([umi_dir batches{1} '.txt']);

    SCData.data = sparse(size(tmp.data,1),384*length(batches));
    SCData.batch = [];
    SCData.well = [];
    for i=1:length(batches)
        tic;
        tmp = importdata([umi_dir batches{i} '.txt']);
        tmp1 = regexp(tmp.textdata(1,:),'\t','split');
        tmp1 = tmp1{1};
        SCData.genes = tmp.textdata(2:end);
        SCData.well = [SCData.well tmp1];
        SCData.data(:,(i-1)*384+1:i*384) = tmp.data;
        SCData.batch = [SCData.batch repmat(batches_numbers(i),1,length(tmp1))];
        fprintf(1,'%s\t %d/%d\n',batches{i},i,length(batches));
        toc;
    end

    SC_well_to_idx = containers.Map();
    for i=1:length(SCData.well)
        SC_well_to_idx(SCData.well{i}) = i;
    end

end