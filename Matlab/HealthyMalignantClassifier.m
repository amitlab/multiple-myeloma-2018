%%
% KNN classifier for healthy PC
%%
K=100;
% Read new mm meta data
f = fopen('metadata_pre_lite.txt');
l = fgetl(f);
meta = textscan(f,'AB%d\tSB%d\t%s\t%s',400);
fclose(f);

% load SCData structure
load SCDataPCFinal;

% add metadata to data structure
batch2patient = containers.Map(meta{1},meta{4});
SCDataPCFinal.patient = {};
for i=1:length(SCDataPCFinal.batch)
    SCDataPCFinal.patient{i} = batch2patient(SCDataPCFinal.batch(i));
end

p = SCDataPCFinal.patient(SCDataPCFinal.cells);
D = SCDataPCFinal.DS_norm_all(ClusterPC.genes_group,:);

% Find all normal cells
control_cells = find(ismember(p,{'hip1','hip2','hip3','hip4','hip5','hip6','hip7','hip8','hip9','hip10','hip11'}));

% Calculate the correlation to the normal cells
normal_corr_dist = [];
for i=1:11
    inds = find(ismember(p,['hip' num2str(i)]));
    rest_inds = setdiff(control_cells,inds); % exculde same patient
    [IDX,Di] = knnsearch(D(:,rest_inds)',D(:,inds)','k',K,'Distance','correlation');

    % append
    normal_corr_dist = [normal_corr_dist; mean(Di,2)];
    fprintf(1,'%d\n',i);
end

% Now for the rest of the patients
patients_names = unique(p(~ismember(p,{'hip1','hip2','hip3','hip4','hip5','hip6','hip7','hip8','hip9','hip10','hip11'})));

% Calculate correlation
patients_corr_dist = zeros(1,length(p));
for i=1:length(patients_names)
    inds = find(ismember(p,patients_names{i}));
    [IDX,Di] = knnsearch(D(:,control_cells)',D(:,inds)','k',K,'Distance','correlation');
    patients_corr_dist(inds) = mean(Di,2);
    fprintf(1,'%d\t%s\n',i,patients_names{i});
end

% Estimate normal distribution
[mu,s]=normfit(normal_corr_dist);

% Use mu and s to calculate the p-value
p_val = log2(1-normcdf(patients_corr_dist,mu,s));
