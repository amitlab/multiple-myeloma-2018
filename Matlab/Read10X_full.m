function [SCData] = Read10X_full(dir_name,min_umis)

    % Read genes
    SCData = {};
    genes = importdata([dir_name '/genes.tsv']);
    SCData.genes = {};
    SCData.ensmbl = {};
    for i=1:length(genes)
        t=regexp(genes{i},'\t','split');
        SCData.ensmbl{i} = t{1};
        SCData.genes{i} = t{2};
    end

     % Read barcodes (for V(D)J)
    barcodes = importdata([dir_name '/barcodes.tsv']);
    if isempty(barcodes)
        fprintf(1,'Error reading file: %s\n',[dir_name '/barcodes.tsv']);
        return;
    end
    
    % Read matrix
    dat = load([dir_name '/matrix.mtx']);
    if isempty(dat)
        fprintf(1,'Error reading file: %s\n',[dir_name '/matrix.mtx']);
        return;
    end
    
    uc = zeros(max(dat(2:end,2)),1);
    for i=1:length(dat(2:end,2))-1
        uc(dat(1+i,2)) = uc(dat(1+i,2)) + dat(1+i,3);
        if (mod(i,100000)==0),fprintf(1,'%d\n',i); end
    end

    cells = find(uc>min_umis);
    cells_inds = zeros(max(dat(2:end,2)),1);
    cells_inds(cells) = 1:length(cells);
    Mat = zeros(length(SCData.genes),length(cells));
    for i=1:length(dat(2:end,2))-1
        if (cells_inds(dat(1+i,2))>0)
            Mat(dat(1+i,1),cells_inds(dat(1+i,2))) = Mat(dat(1+i,1),cells_inds(dat(1+i,2))) + dat(1+i,3);
        end
        if (mod(i,100000)==0),fprintf(1,'%d\n',i); end
    end
    
    SCData.data = Mat;
    SCData.barcodes = barcodes(cells);
    % Read barcodes
    %barcodes = importdata([dir_name '/barcodes.tsv']);
    %for i=1:length(barcodes)
    %    SCData.cell_barcode{i} = barcodes{i}(1:7);
    %   SCData.batch_barcode{i} = barcodes{i}(8:11);
  
end
