function [SCData] = Read10X(dir_name)

    % Read matrix
    dat = load([dir_name '/matrix.mtx']);
    if isempty(dat)
        fprintf(1,'Error reading file: %s\n',[dir_name '/matrix.mtx']);
        return;
    end
    Mat = sparse(dat(2:end,1),dat(2:end,2),dat(2:end,3),dat(1,1),dat(1,2));
    SCData = {};
    SCData.data = Mat;
    
    % Read genes
    genes = importdata([dir_name '/genes.tsv']);
    SCData.genes = {};
    SCData.ensmbl = {};
    for i=1:length(genes)
        t=regexp(genes{i},'\t','split');
        SCData.ensmbl{i} = t{1};
        SCData.genes{i} = t{2};
    end
    
    % Read barcodes
    barcodes = importdata([dir_name '/barcodes.tsv']);
    for i=1:length(barcodes)
        SCData.cell_barcode{i} = barcodes{i}(1:7);
        SCData.batch_barcode{i} = barcodes{i}(8:11);
    end
end
