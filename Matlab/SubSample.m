function [SCData] = SubSample(SCData,min_umi,sample_molecules)
    eg=strmatch('ERCC',SCData.genes);
    allg = setdiff(1:length(SCData.genes),eg);
    cells = sum(SCData.data(allg,:))>min_umi;
    DS = SCData.data(:,cells);
    DS(eg,:) = 0;
    for i=1:size(DS,2)
        DS(:,i) = downsample(DS(:,i),sample_molecules,100);
        if (mod(i,100)==0),fprintf(1,'%d\n',i);end
    end
    DS_norm_all = log2((DS+1e-4)./repmat(mean(DS,2)+1e-4,1,sum(cells)));
    SCData.cells = cells;
    SCData.DS = DS;
    SCData.DS_norm_all = DS_norm_all;
end
    
