% Run phenograph clustering
% n_genes - to use as markers
% n_neighbors - for knn tree
% n_cycles - iterations to refine gene list
function [ClusterRes] = RunPhenographClustering(SCData,n_genes,n_neighbors,n_cycels)
    IDXfull = ones(1,size(SCData.DS,2));
    genes_group = group_separate_genes_first_ds(n_genes,IDXfull,SCData,0.01);
    
    for i=1:n_cycels
        [labels,communities,G2,uniqueID] = phenograph(log2(1+SCData.DS(genes_group,:))', n_neighbors);
        IDXfull = labels';
        genes_group = getGeneGroup(n_genes,IDXfull,SCData);
    end

    ClusterRes = {};
    [ClusterRes.labels,ClusterRes.communities,ClusterRes.G2,~] = phenograph(log2(1+SCData.DS(genes_group,:))', n_neighbors);
    ClusterRes.genes_group=genes_group;
end
