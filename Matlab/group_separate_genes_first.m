function [final_genes_group] = group_separate_genes_first(n_genes,MAP,SCData,exp_cutoff,black_list)
	% Find genes which separate the groups

	inds = find(~isnan(MAP));

	var_mean=var(SCData.data(:,inds),0,2) ./ mean(SCData.data(:,inds),2);

	[tmp order] = sort(var_mean,'descend');
	final_genes_group = order(1:n_genes);

	% Filter genes
	to_filter_inds = [];
	for i=1:length(black_list)
	    to_filter_inds = union(to_filter_inds,...
	        find(~cellfun('isempty',regexp(SCData.genes,black_list{i},'match'))));
	end

	final_genes_group = setdiff(final_genes_group,to_filter_inds);
	[tmp order] = sort(var_mean(final_genes_group),'descend');
	final_genes_group = final_genes_group(order);
