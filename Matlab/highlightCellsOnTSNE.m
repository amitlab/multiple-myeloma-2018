function [] = highlightCellsOnTSNE(mapped,cells,msize,color)
    figure;
    scatter(mapped(:,1),mapped(:,2),msize,'filled','markerfacecolor',[0.7 0.7 0.7]);
    hold on;
    scatter(mapped(cells,1),mapped(cells,2),msize,'filled','markerfacecolor',color);
    title(['n = ' num2str(sum(cells))]);
    alpha 0.4;
end