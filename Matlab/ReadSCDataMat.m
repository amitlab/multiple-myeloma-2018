function [SCData] = ReadSCDataMat(batches_numbers,umi_dir)

batches = arrayfun(@strcat,repmat({'AB'},length(batches_numbers),1),strread(num2str(batches_numbers),'%s'));

tmp = load([umi_dir batches{1} '.mat']);

%SCData.data = zeros(size(tmp.data,1),384*length(batches));
SCData.data = sparse(length(tmp.Batch.genes),384*length(batches));
SCData.batch = [];
SCData.well = [];
for i=1:length(batches)
    tic;
    tmp = load([umi_dir batches{i} '.mat']);
    SCData.genes = tmp.Batch.genes;
    SCData.well = [SCData.well tmp.Batch.well];
    SCData.data(:,(i-1)*384+1:i*384) = tmp.Batch.data;
    SCData.batch = [SCData.batch tmp.Batch.batch];
    fprintf(1,'%s\t %d/%d\n',batches{i},i,length(batches));
    toc;
end

end