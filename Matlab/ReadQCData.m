% Read MARS_seq QC data
% QC_dir - directory of MARS-seq QC 
function QCData = ReadQCData(batches_numbers,varargin)

	args = parse_namevalue_pairs(struct('QC_dir', '/Volumes/eyald/sc_pipeline_strict/scdb_hisat_mouse/output/QC/read_stats/'),varargin);

	QC_dir = args.QC_dir;

	batches = arrayfun(@strcat,repmat({'AB'},length(batches_numbers),1),strread(num2str(batches_numbers),'%s'));

	QCData.data = [];
	QCData.batch = [];
	QCData.well = [];
	for i=1:length(batches)
	    tmp = importdata([QC_dir batches{i} '.txt']);
	    QCData.header = tmp.textdata(1,2:end);
	    QCData.well = [QCData.well tmp.textdata(2:end,1)];
	    QCData.data = [QCData.data; tmp.data];
	    QCData.batch = [QCData.batch repmat(batches_numbers(i),1,size(tmp.data,1))];
	    fprintf(1,'%s\n',batches{i});
	end
end