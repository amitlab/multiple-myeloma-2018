function [SCDataOut] = SubMat(SCDataIn,cells_to_use)
    SCDataOut = SCDataIn;
    c_inds = find(SCDataOut.cells);
    SCDataOut.cells(1:end) = logical(0);
    SCDataOut.cells(c_inds(cells_to_use)) = logical(1);
    SCDataOut.DS = SCDataOut.DS(:,cells_to_use);
    SCDataOut.DS_norm_all = SCDataOut.DS_norm_all(:,cells_to_use);
end