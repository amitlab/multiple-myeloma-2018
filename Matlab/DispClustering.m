function [genes_perm, cells_perm, cols_cluster_perm] = DispClustering(D,IDX,varargin)
% plot clustering as heatmap
% 
% Arguments:
%  D   - NxM matrix with N genes and M cells.
%  IDX - clustering of cells
%
% name/value arguments:
%  clrs - colormap.
%  ordercluster - true (defaults) to order clusters.
%  cluster_genes - true if run K-means on genes
%  genes_k       - K group for genes
%  clustering    - k (kmeans) or hirarchical
%  gname         - Gene names
% Returns:
%  rows_perm - genes order
%  cols_perm - cells order
%  cols_cluster_perm - cells cluster order

    show_average = true;
    [Ngenes,Ncells] = size(D);

    args = struct('clrs',jet, 'ordercluster', true,...
                  'cluster_genes', true, 'genes_k', 10,'clustering','hirarchical','gname','');
    args = parse_namevalue_pairs(args, varargin);

    avg_in_clusters = zeros(Ngenes,length(unique(IDX)));
    c = unique(IDX);
    numClusters = length(c);
    for i=1:length(c)
        avg_in_clusters(:,i) = log2(mean(2.^D(:,IDX==c(i)),2));
    end
    if (args.ordercluster)
        Y = pdist(avg_in_clusters','correlation');
        Z = linkage(Y,'average');
        figure;
        [H,T,cols_cluster_perm] = dendrogram(Z,0,'colorthreshold','default');
        close;
    else
        cols_cluster_perm = 1:numClusters;
    end

    if (args.cluster_genes)
        if (strcmp(args.clustering,'hirarchical'))
            Y = pdist(D,'correlation');
            %Y = pdist(D);
            Z = linkage(Y,'complete');
            [H,T,genes_perm] = dendrogram(Z,0,'colorthreshold','default');
            close;
        else
            opts = statset('Display','off','maxIter',1000);
            [IDX_genes,C] = kmeans(D,args.genes_k,'replicates',50,'options',opts);
            Y = pdist(C,'correlation');
            Z = linkage(Y,'complete');
            figure;
            [H,T,clusters_perm] = dendrogram(Z,0,'colorthreshold','default');
            close;
        end
    else
        clusters_perm = 1:Ncells;
    end

    IDX1 = IDX;
    for i=1:length(unique(IDX))
        IDX1(IDX==cols_cluster_perm(end-i+1)) = i;
    end
    [cells_o, cells_perm] = sort(IDX1,'descend');

    for i=1:length(unique(cells_o))
        j = find(cells_o==i);
        jr = j(randperm(length(j)));
        cells_perm(j) = cells_perm(jr);
    end

    if (args.cluster_genes)
        if (strcmp(args.clustering,'hirarchical'))
            genes_perm = genes_perm;
        else
            IDX1 = IDX_genes;
            for i=1:length(unique(IDX_genes))
                IDX1(IDX_genes==clusters_perm(i)) = i;
            end
            [genes_o, genes_perm] = sort(IDX1,'descend');
        end
    else
        genes_perm = 1:Ngenes;
    end
    AvgD = avg_in_clusters(genes_perm,cols_cluster_perm);
    D = D(genes_perm,cells_perm);
    figure;

    if (show_average)
    subplot(1,10,1:2);
    h1=imagesc(AvgD,[0 10]);
    hold on;
    colormap(args.clrs);
    if (args.cluster_genes && strcmp(args.clustering,'kmeans'))

        a=hist(genes_o,1:size(C,1));
        b = cumsum(a(end:-1:1));
        hold on;
        for i=1:length(b)-1
            plot([0 numClusters+1],[b(i)+0.5 b(i)+0.5],'-k','LineWidth',0.5);
        end
    end
    for i=1:numClusters
        plot([i+0.5,i+0.5],[0 Ngenes+1],'-k','LineWidth',0.5);
    end
    set(gca,'ytick',[],'xtick',1:numClusters);
    if (~isempty(args.gname))
        set(gca,'ytick',1:length(genes_perm),'yticklabel',args.gname(genes_perm));
    end
    end

    if (show_average)
    subplot(1,10,3:10);
    end
    h2=imagesc(D,[0 10]);
    colormap(args.clrs);
    if (args.cluster_genes && strcmp(args.clustering,'kmeans'))

        a=hist(genes_o,1:size(C,1));
        b = cumsum(a(end:-1:1));
        hold on;
        for i=1:length(b)-1
            plot([0 size(C,2)+1],[b(i)+0.5 b(i)+0.5],'-k','LineWidth',0.5);
        end
    end
    a=hist(cells_o,1:size(avg_in_clusters,1));
    b = cumsum(a(end:-1:1));
    hold on;
    for i=1:length(b)-1
        plot([b(i)+0.5 b(i)+0.5],[0 size(avg_in_clusters,1)+1],'-k','LineWidth',0.5);
    end

    set(gca,'ytick',[]);
