function D = downsample(vec,n,rep)
    n_samples = 1;
    a = zeros(1,sum(vec));
    pos = 1;
    j = find(vec);
    for i=1:length(j)
        a(pos:pos+vec(j(i))-1) = i;
        pos = pos + vec((j)i);
    end
    D = zeros(size(vec));
    for i=1:n_samples
        D = D+histcounts(randsample(repmat(a,1,rep),n),0.5:1:length(vec)+1)';
    end
    D = D/n_samples;
end