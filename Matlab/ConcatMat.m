function [SCDataOut] = ConcatMat(SCData1,SCData2)
    SCDataOut = SCData1;
    SCDataOut.data = [SCData1.data SCData2.data];
    SCDataOut.batch = [SCData1.batch SCData2.batch];
    SCDataOut.well = [SCData1.well SCData2.well];
    SCDataOut.cells = [SCData1.cells SCData2.cells];
    SCDataOut.DS = [SCData1.DS SCData2.DS];
    SCDataOut.DS_norm_all = [SCData1.DS_norm_all SCData2.DS_norm_all];
    SCDataOut.concatOrigin = [ones(1,size(SCData1.DS,2)) 2*ones(1,size(SCData2.DS,2))];
end